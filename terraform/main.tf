resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "main"
  }
}

resource "aws_subnet" "Public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Public"
  }
}

resource "aws_subnet" "Public1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Public1"
  }
}

resource "aws_subnet" "Private" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Private"
  }
}

resource "aws_subnet" "Private1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Private1"
  }
}

resource "aws_internet_gateway" "test_gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "test_gw"
  }
}

resource "aws_eip" "test_eip" {
  domain = "vpc"
}

resource "aws_nat_gateway" "example" {
  allocation_id = aws_eip.test_eip.id
  subnet_id     = aws_subnet.Public.id
}

resource "aws_route_table" "test_rtb" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.test_gw.id
  }

  tags = {
    Name = "Public-rtb"
  }
}

resource "aws_route_table_association" "Public-subnet1" {
  subnet_id      = aws_subnet.Public.id
  route_table_id = aws_route_table.test_rtb.id
}

resource "aws_route_table_association" "Public-subnet2" {
  subnet_id      = aws_subnet.Public1.id
  route_table_id = aws_route_table.test_rtb.id
}

resource "aws_route_table" "Private_rtb" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.example.id
  }

  tags = {
    Name = "Private-rtb"
  }
}

resource "aws_route_table_association" "Private-subnet1" {
  subnet_id      = aws_subnet.Private.id
  route_table_id = aws_route_table.test_rtb.id
}

resource "aws_route_table_association" "Private-subnet2" {
  subnet_id      = aws_subnet.Private1.id
  route_table_id = aws_route_table.test_rtb.id
}

resource "aws_instance" "faiz" {
  ami           = "ami-0c7217cdde317cfec"
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}